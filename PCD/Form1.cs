﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PCD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "jpg(*.jpg)|*.jpg|bmp(*.bmp)|*.bmp";
            if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName.Length > 0)
            {
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox1.Image = Image.FromFile(ofd.FileName);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "jpg(*.jpg)|*.jpg|bmp(*.bmp)|*.bmp";
            if (sfd.ShowDialog() == DialogResult.OK && sfd.FileName.Length > 0)
            {
                pictureBox1.Image.Save(sfd.FileName);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bmp1 = new Bitmap(200, 140);
            for (int y = 0; y < 70; y++)
            for (int x = 0; x < 200; x++)
                bmp1.SetPixel(x, y, ImageProcessing.Red);
            for (int y = 70; y < 140; y++)
            for (int x = 0; x < 200; x++)
                bmp1.SetPixel(x, y, ImageProcessing.White);
            pictureBox2.Image = bmp1;
            int number1, number2, number3;
            Int32.TryParse(textBox1.Text, out number1);
            Int32.TryParse(textBox2.Text, out number2);
            Int32.TryParse(textBox3.Text, out number3);

            Bitmap bmpCampur = new Bitmap(100, 100);
            Bitmap bmpMerah = new Bitmap(100, 100);
            Bitmap bmpHijau = new Bitmap(100, 100);
            Bitmap bmpBiru = new Bitmap(100, 100);

            Color mix = Color.FromArgb(number1, number2, number3);

            for (int i = 0; i < 100; i++)
            for (int j = 0; j < 100; j++)
            {
                bmpCampur.SetPixel(j, i, mix);
                bmpMerah.SetPixel(j, i, ImageProcessing.Red);
                bmpHijau.SetPixel(j, i, ImageProcessing.Green);
                bmpBiru.SetPixel(j, i, ImageProcessing.Blue);
            }

            boxColor1.Image = bmpMerah;
            boxColor2.Image = bmpHijau;
            boxColor3.Image = bmpBiru;
            boxColor4.Image = bmpCampur;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "jpg(*.jpg)|*.jpg|bmp(*.bmp)|*.bmp";
            if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName.Length > 0)
            {
                pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox3.Image = Image.FromFile(ofd.FileName);
            }
        }

        private void btnGray_Click(object sender, EventArgs e)
        {
            pictureBox6.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox6.Image = ImageProcessing.Grayscale((Bitmap) pictureBox3.Image);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            pictureBox7.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox7.Image = ImageProcessing.Binerize((Bitmap) pictureBox3.Image, 128);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            pictureBox8.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox8.Image = ImageProcessing.Brightness((Bitmap) pictureBox3.Image, 30);
        }

        private void btnFlipH_Click(object sender, EventArgs e)
        {
            pictureBox4.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox4.Image = ImageProcessing.Flip((Bitmap) pictureBox3.Image, ImageProcessing.FlipType.Horizontal);
        }

        private void btnFlipV_Click(object sender, EventArgs e)
        {
            pictureBox5.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox5.Image = ImageProcessing.Flip((Bitmap) pictureBox3.Image, ImageProcessing.FlipType.Vertikal);
        }

        private void tabPage7_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "jpg(*.jpg)|*.jpg|bmp(*.bmp)|*.bmp";
            if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName.Length > 0)
            {
                pictureBox9.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox9.Image = Image.FromFile(ofd.FileName);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pictureBox10.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox10.Image = ImageProcessing.Kuantisasi((Bitmap) pictureBox9.Image, 16);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            pictureBox11.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox11.Image = ImageProcessing.Kuantisasi((Bitmap)pictureBox9.Image, 4);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            pictureBox12.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox12.Image = ImageProcessing.Kuantisasi((Bitmap)pictureBox9.Image, 2);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "jpg(*.jpg)|*.jpg|bmp(*.bmp)|*.bmp";
            if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName.Length > 0)
            {
                pictureBox13.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox13.Image = Image.FromFile(ofd.FileName);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            pictureBox14.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox14.Image = ImageProcessing.Brightness((Bitmap)pictureBox13.Image, 50);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            pictureBox15.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox15.Image = ImageProcessing.Contrast((Bitmap)pictureBox13.Image, 0.7f);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "jpg(*.jpg)|*.jpg|bmp(*.bmp)|*.bmp";
            if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName.Length > 0)
            {
                pictureBox16.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox16.Image = Image.FromFile(ofd.FileName);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            pictureBox17.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox17.Image = ImageProcessing.Negative((Bitmap)pictureBox16.Image);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            pictureBox17.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox17.Image = ImageProcessing.Logaritmic((Bitmap)pictureBox16.Image, 50);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            pictureBox22.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox22.Image = ImageProcessing.InversLog((Bitmap)pictureBox17.Image, 50);
        }

        private void button15_Click(object sender, EventArgs e)
        {

        }
    }
}