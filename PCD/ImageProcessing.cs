﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCD
{
    class ImageProcessing
    {
        public static readonly Color Red = Color.FromArgb(255, 0, 0);
        public static readonly Color Green = Color.FromArgb(0, 255, 0);
        public static readonly Color Blue = Color.FromArgb(0, 0, 255);
        public static readonly Color White = Color.FromArgb(255, 255, 255);
        public static readonly Color Black = Color.FromArgb(0, 0, 0);

        public enum FlipType
        {
            Horizontal = 1,
            Vertikal = 2
        };

        public static Bitmap Grayscale(Bitmap bmp)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int m = (tmp.GetPixel(x, y).R + tmp.GetPixel(x, y).G + tmp.GetPixel(x, y).B) / 3;
                tmp.SetPixel(x, y, Color.FromArgb(m, m, m));
            }
            return tmp;
        }

        public static Bitmap Binerize(Bitmap bmp, int threshold)
        {
            Bitmap tmp = Grayscale(bmp);
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int m = tmp.GetPixel(x, y).R;
                m = m < threshold ? 0 : 255;
                tmp.SetPixel(x, y, Color.FromArgb(m, m, m));
            }
            return tmp;
        }

        public static Bitmap Brightness(Bitmap bmp, int k)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r = (tmp.GetPixel(x, y).R + k) > 255 ? 255 : (tmp.GetPixel(x, y).R + k);
                int g = (tmp.GetPixel(x, y).G + k) > 255 ? 255 : (tmp.GetPixel(x, y).G + k);
                int b = (tmp.GetPixel(x, y).B + k) > 255 ? 255 : (tmp.GetPixel(x, y).B + k);
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return tmp;
        }

        public static Bitmap Flip(Bitmap bmp, FlipType ft)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            Debug.WriteLine("Height : " + bmp.Height);
            Debug.WriteLine("Width : " + bmp.Width);
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                Color c = new Color();
                switch (ft)
                {
                    case FlipType.Horizontal:
                        c = bmp.GetPixel(bmp.Width - 1 - x, y);
                        break;
                    case FlipType.Vertikal:
                        c = bmp.GetPixel(x, bmp.Height - 1 - y);
                        break;
                }
                tmp.SetPixel(x, y, Color.FromArgb(c.R, c.G, c.B));
            }
            return tmp;
        }

        public static Bitmap Kuantisasi(Bitmap bmp, int k)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            int th = 256 / k;
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int m = (tmp.GetPixel(x, y).R + tmp.GetPixel(x, y).G + tmp.GetPixel(x, y).B) / 3;
                int q = th * (m / th);
                tmp.SetPixel(x, y, Color.FromArgb(q, q, q));
            }
            return tmp;
        }

        public static Bitmap Contrast(Bitmap bmp, float k)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
            {
                int r = (int) ((tmp.GetPixel(x, y).R * k) > 255 ? 255 : (tmp.GetPixel(x, y).R * k));
                int g = (int) ((tmp.GetPixel(x, y).G * k) > 255 ? 255 : (tmp.GetPixel(x, y).G * k));
                int b = (int) ((tmp.GetPixel(x, y).B * k) > 255 ? 255 : (tmp.GetPixel(x, y).B * k));
                tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            return bmp;
        }

        public static Bitmap Negative(Bitmap bmp)
        {
            Bitmap tmp = (Bitmap) bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
            for (int x = 0; x < bmp.Width; x++)
                tmp.SetPixel(x, y,
                    Color.FromArgb(255 - tmp.GetPixel(x, y).R, 255 - tmp.GetPixel(x, y).G, 255 - tmp.GetPixel(x, y).B));
            return bmp;
        }
        public static Bitmap Logaritmic(Bitmap bmp, int k)
        {
            Bitmap tmp = (Bitmap)bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
                for (int x = 0; x < bmp.Width; x++)
                {
                    int r = (int)(k * Math.Log(tmp.GetPixel(x, y).R,10));
                    int g = (int)(k * Math.Log(tmp.GetPixel(x, y).G, 10));
                    int b = (int)(k * Math.Log(tmp.GetPixel(x, y).B, 10));
                    tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
                }
            return bmp;
        }

        public static Bitmap InversLog(Bitmap bmp, int k)
        {
            Bitmap tmp = (Bitmap)bmp.Clone();
            for (int y = 0; y < bmp.Height; y++)
                for (int x = 0; x < bmp.Width; x++)
                {
                    int r = (int)(10 * Math.Exp(tmp.GetPixel(x, y).R/ k));
                    int g = (int)(10 * Math.Exp(tmp.GetPixel(x, y).G / k));
                    int b = (int)(10 * Math.Exp(tmp.GetPixel(x, y).B / k));
                    tmp.SetPixel(x, y, Color.FromArgb(r, g, b));
                }
            return bmp;
        }
    }
}